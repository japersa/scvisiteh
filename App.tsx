import React, {useState, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
  BackHandler,
  Dimensions,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Loader from './Loader';
const {width, height} = Dimensions.get('window');

const App = () => {
  const mapView = useRef(null);

  const [origin, setOrigin] = useState(null);
  const [destination, setDestination] = useState(null);

  const GOOGLE_MAPS_APIKEY = 'AIzaSyBMesj43pD3-xSRRgK7lZLg3kAs3Or6QeQ';

  const [showLoading, setShowLoading] = useState(false);
  const [identification, setIdentification] = useState('');
  const [inputError, setInputError] = useState(false);
  const [result, setResult] = useState(null);
  const [region, setRegion] = useState({
    latitude: 10.494762,
    longitude: -75.123706,
    latitudeDelta: 0.001,
    longitudeDelta: 0.001,
  });

  const [inputFields, setInputFields] = useState([
    {
      id: 'identificacion',
      type: 'text',
      placeholder: 'Identificación',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Primer nombre',
      type: 'text',
      placeholder: 'Primer nombre',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Segundo nombre',
      type: 'text',
      placeholder: 'Segundo nombre',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Primer apellido',
      type: 'text',
      placeholder: 'Primer apellido',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Segundo apellido',
      type: 'text',
      placeholder: 'Segundo apellido',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Nacionalidad',
      type: 'text',
      placeholder: 'Nacionalidad',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Fecha de nacimiento',
      type: 'text',
      placeholder: 'Fecha de nacimiento',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Sexo',
      type: 'text',
      placeholder: 'Sexo',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Teléfono',
      type: 'text',
      placeholder: 'Teléfono',
      required: true,
      error: false,
      value: '',
    },
    {
      id: 'Dirección',
      type: 'text',
      placeholder: 'Dirección',
      required: true,
      error: false,
      value: '',
    },
  ]);

  const closeApp = () => {
    Alert.alert(
      'Salir de la aplicación',
      '¿Quieres salir?',
      [
        {
          text: 'Cancelar',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Salir', onPress: () => BackHandler.exitApp()},
      ],
      {cancelable: false},
    );
  };

  const getFormattedDate = (date: Date) => {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
  };

  const showMessage = (title: string, message: string) => {
    setTimeout(() => {
      Alert.alert(title, message);
    }, 200);
  };

  const onReady = (result: {coordinates: any}) => {
    if (mapView != null) {
      mapView.current.fitToCoordinates(result.coordinates, {
        edgePadding: {
          right: width / 10,
          bottom: height / 10,
          left: width / 10,
          top: height / 10,
        },
      });
    }
  };

  const onSubmit = () => {
    setResult(null);
    setRegion(null);
    setOrigin(null);
    setDestination(null);
    if (identification === '') {
      setInputError(true);
    } else {
      try {
        setShowLoading(true);
        const body = JSON.stringify({
          identificacion: identification,
        });

        fetch('http://scvisiteh.com:3000/consultas/buscarIdentificacion', {
          method: 'POST',
          body: body,
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then(response => response.json())
          .then(responseJson => {
            const {results} = responseJson;
            setShowLoading(false);

            if (Array.isArray(results) && results.length === 0) {
              showMessage(
                'Registro no existe',
                'Por favor ingresa un documento de identidad registrado',
              );
            } else {
              setResult(results);
              setInputFields([
                {
                  id: 'identificacion',
                  type: 'text',
                  placeholder: 'Identificación',
                  required: true,
                  error: false,
                  value: results.identificacion,
                },
                {
                  id: 'Primer nombre',
                  type: 'text',
                  placeholder: 'Primer nombre',
                  required: true,
                  error: false,
                  value: results.nombre1,
                },
                {
                  id: 'Segundo nombre',
                  type: 'text',
                  placeholder: 'Segundo nombre',
                  required: true,
                  error: false,
                  value: results.nombre2,
                },
                {
                  id: 'Primer apellido',
                  type: 'text',
                  placeholder: 'Primer apellido',
                  required: true,
                  error: false,
                  value: results.apellido1,
                },
                {
                  id: 'Segundo apellido',
                  type: 'text',
                  placeholder: 'Segundo apellido',
                  required: true,
                  error: false,
                  value: results.apellido2,
                },
                {
                  id: 'Nacionalidad',
                  type: 'text',
                  placeholder: 'Nacionalidad',
                  required: true,
                  error: false,
                  value: results.pais,
                },
                {
                  id: 'Fecha de nacimiento',
                  type: 'text',
                  placeholder: 'Fecha de nacimiento',
                  required: true,
                  error: false,
                  value: getFormattedDate(new Date(results.date)),
                },
                {
                  id: 'Sexo',
                  type: 'text',
                  placeholder: 'Sexo',
                  required: true,
                  error: false,
                  value: results.sexo,
                },
                {
                  id: 'Teléfono',
                  type: 'text',
                  placeholder: 'Teléfono',
                  required: true,
                  error: false,
                  value: results.telefono,
                },
                {
                  id: 'Dirección',
                  type: 'text',
                  placeholder: 'Dirección',
                  required: true,
                  error: false,
                  value: results.direccion,
                },
              ]);

              setRegion({
                latitude: results.lat1,
                longitude: results.lon1,
                latitudeDelta: 0.015 * 5,
                longitudeDelta: 0.0121 * 5,
              });

              setOrigin({
                latitude: results.lat1,
                longitude: results.lon1,
              });

              setDestination({
                latitude: results.lat2,
                longitude: results.lon2,
              });
            }
          })
          .catch(error => {
            setShowLoading(false);
            showMessage('Error', error);
          });
      } catch (error) {
        console.log('Error', error);
      }
    }
  };

  const changeValueInput = (value: any) => {
    if (value !== '') {
      setInputError(false);
    }
    setIdentification(value);
  };

  return (
    <>
      <Loader
        visible={showLoading}
        isModal={true}
        isHUD={true}
        color={'#FFFFFF'}
        hudColor={'#000000'}
        barHeight={64}
        size={60}
      />
      <View style={styles.container}>
        <SafeAreaView />
        <ScrollView>
          <View style={styles.card}>
            <View style={styles.containerTitleCard}>
              <Text style={styles.cardTitle}>Buscar</Text>
            </View>
            <View style={styles.form}>
              <Text style={styles.textInput}>
                Identificación a consultar
                <Text style={styles.requiredLabel}>*</Text>
              </Text>
              <TextInput
                style={[styles.input, inputError && styles.inputError]}
                value={identification}
                placeholder={'Ej. 00000000'}
                placeholderTextColor={'#3f3f3f'}
                keyboardType={'numeric'}
                onChangeText={changeValueInput}
              />
            </View>
            <View style={styles.containerBottomCard}>
              <View style={styles.containerButton}>
                <TouchableOpacity style={styles.button} onPress={onSubmit}>
                  <Text style={styles.textButton}>Buscar</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.buttonCancel}
                  onPress={closeApp}>
                  <Text style={styles.textButtonCancel}>Salir</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {result != null && (
            <View>
              <View style={{height: 300}}>
                <MapView
                  provider={PROVIDER_GOOGLE}
                  style={{flex: 1}}
                  onMapReady={() => {
                    console.log('onMapReady');
                  }}
                  initialRegion={region}
                  ref={mapView}
                  onRegionChangeComplete={() => {
                    console.log('onRegionChange');
                  }}>
                  <Marker coordinate={origin} />
                  <Marker coordinate={destination} />
                  <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GOOGLE_MAPS_APIKEY}
                    strokeWidth={3}
                    strokeColor="hotpink"
                    onReady={onReady}
                    onError={errorMessage => {
                      console.log(errorMessage);
                    }}
                    mode={'DRIVING'}
                  />
                </MapView>
              </View>
              <View style={styles.card}>
                <View style={styles.containerTitleCard}>
                  <Text style={styles.cardTitle}>Datos personales</Text>
                </View>
                <View style={styles.form}>
                  {inputFields.map((input, key) => (
                    <View style={styles.inputContainer} key={key}>
                      <Text style={styles.textInput}>{input.placeholder}</Text>
                      <TextInput
                        style={[styles.input]}
                        value={input.value}
                        placeholder={''}
                        editable={false}
                        placeholderTextColor={'#3f3f3f'}
                      />
                    </View>
                  ))}
                </View>
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  input: {
    height: 44,
    color: '#3F3F3F',
    backgroundColor: '#ededed',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  inputError: {
    height: 44,
    color: '#3F3F3F',
    borderWidth: 1,
    borderColor: '#ee5435',
    borderRadius: 4,
    paddingStart: 10,
    paddingEnd: 10,
  },
  textInput: {
    color: '#8A8A8A',
    marginBottom: 10,
    fontSize: 11,
  },
  inputContainer: {
    marginBottom: 16,
  },
  requiredLabel: {
    color: 'red',
  },
  button: {
    marginBottom: 8,
    marginTop: 8,
    marginRight: 16,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#1a5584',
    borderRadius: 5,
  },
  textButton: {
    textTransform: 'uppercase',
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  card: {
    margin: 16,
    borderRadius: 4,
    shadowColor: '#19000000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: '#ffffff',
  },
  containerTitleCard: {
    borderBottomWidth: 1,
    borderBottomColor: '#0000000d',
  },
  containerBottomCard: {
    borderTopWidth: 1,
    borderTopColor: '#0000000d',
  },
  cardTitle: {
    padding: 16,
    textTransform: 'uppercase',
    fontSize: 14,
    color: '#4f4f4f',
    fontWeight: '600',
    letterSpacing: 0.32,
  },
  form: {
    padding: 16,
  },
  buttonCancel: {
    marginBottom: 8,
    marginTop: 8,
    marginRight: 16,
    justifyContent: 'center',
    height: 30,
    width: 100,
    backgroundColor: '#bebebe',
    borderRadius: 5,
  },
  textButtonCancel: {
    textTransform: 'uppercase',
    color: '#3f3f3f',
    textAlign: 'center',
    fontSize: 12,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0.16,
  },
  containerButton: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
});

export default App;
